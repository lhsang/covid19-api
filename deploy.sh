#!/bin/bash
set -eux;

if [ $# -eq 0 ]; then
    echo "Path to deploy is missing"
    exit 1
else
    dir="$1"
fi

cd $dir

git fetch
git reset --hard
git checkout master
git pull

echo "Delete *.pyc in ${dir}"
find $dir -name \*.pyc -delete
echo "Delete __pycache__ in ${dir}"
find $dir -name __pycache__ -empty -delete

cd app/
.venv/bin/pip install -r requirements.txt

.venv/bin/python -m pytest tests/

sudo service covid19-api restart
